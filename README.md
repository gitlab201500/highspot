# Playlist manager

Usage: java -jar plmgr-assembly-1.0.jar mixtape.json changes.json output.json<br/>
mixtape.json - initial playlist list<br/>
changes.json - changes to be applied<br/>
output.json - resulting playlist  <br/>


## Changes.json format
```
{
  "changes":[
    {"add_song":{"id":"40","playlist_id":"1"}},
    {"add_playlist":{"id":"100","user_id":"1","song_ids":["1","2","3"]}},
    {"rm_playlist":{"id":"98"}}
  ]
}
```

## Prerequisites

Implemented in Scala.<br/>
jvm 1.8 is required.<br/>
Sbt is required to build binary and run tests.<br/>
To install sbt follow the link: (https://www.scala-sbt.org/release/docs/Setup.html)<br/>

## Building
```
$ sbt
sbt> assembly
...
[info] Packaging your_path/projects/highspot/target/scala-2.12/plmgr-assembly-1.0.jar ...
```

## Running the tests
```
$ sbt
sbt> test
```

## Running binary
```
java -jar path_to_jar/plmgr-assembly-1.0.jar mixtape.json changes.json output.json
```

## Download binary
Prebuilt jar can be found at (https://drive.google.com/file/d/1C6JE09jUpGuW8h46wlwQSa-toiRyrO2j/view?usp=sharing)

## Implementation Notes
The implementation assumes that both playlists and change log are large files or streams
Thus it treats both as streams and applies charges via merge. This is similar to a merge sort when both lists are sorted
by playlist_ids and then merged by this key. Changelog also preserves initial chronological order of operations. For example
if changelog is [{pl1,a1},{pl2,a2},{pl1,a3},{pl3,a4}], then changelog "stream" will be "sharded" or grouped by playlist_id like this:
[{pl1,a1},{pl1,a3},{pl2,a2}{pl3,a4}] with order of actions preserved. In real world scenario both playlist stream and changelog can be sharded
as a preprocessing step, in this implementaiton it happens in the code.<br/>

This approach also allows running sharded stream in parallel on multiple cores. Since changelog action doesn't span accross playlists
the whole input can be split into shards, one for each core and run in parallel. This is not implemented.<br/>

This implementaiton assumes that users and songs are read-only catalogs and can be stored in SQLDb with some caching for fast access.
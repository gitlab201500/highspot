package test

import plmgr.PlayListMgr.{changeLog, dataset}
import plmgr._

class PlaylistMgrTest extends org.scalatest.FunSuite {
  val mixtape = "mixtape.json"


  test("Add songs to existing playlist") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(AddSong("1", "1"), AddSong("8", "1"), AddSong("3", "1")))
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    val r = logProcessor.result
    val songs = r.playlists
      .find(_.id == "1")
      .get
      .song_ids
      .sorted
    val changes = (dataset.playlists.find(_.id == "1").get.song_ids ++ Seq("1","3")).sorted
    assert(
      songs.corresponds(changes)(_ == _)
    )
  }

  test("Add same song twice") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(AddSong("1", "1"), AddSong("1", "1")))
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    val r = logProcessor.result
    val songs = r.playlists
      .find(_.id == "1")
      .get
      .song_ids
      .sorted
    val changes = (dataset.playlists.find(_.id == "1").get.song_ids ++ Seq("1")).sorted
    assert(
      songs.corresponds(changes)(_ == _)
    )
  }

  test("Add non-existing song") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(AddSong("1", "1"), AddSong("11111111","1")))
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    val r = logProcessor.result
    val songs = r.playlists
      .find(_.id == "1")
      .get
      .song_ids
      .sorted
    val changes = (dataset.playlists.find(_.id == "1").get.song_ids ++ Seq("1")).sorted
    assert(
      songs.corresponds(changes)(_ == _)
    )
  }

  test("Add playlist") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(AddPlaylist("1234", "1", Seq("1")), AddPlaylist("111111", "1", Seq("1","3"))))
    val changeIds = changeLog.changes.map(_.asInstanceOf[AddPlaylist].id)
    val expected = Seq(Playlist("1234", "1", Seq("1")), Playlist("111111", "1", Seq("1","3"))).sortBy(_.id)
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    val r = logProcessor.result
    val res = (r.playlists.filter(p=>changeIds.contains(p.id))).sortBy(_.id)
    assert(
      res.corresponds(expected)(_ == _)
    )
  }
  test("Add same playlist twice") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(AddPlaylist("1234", "1", Seq("1")), AddPlaylist("111111", "1", Seq("1","3")), AddPlaylist("111111", "1", Seq("1","3","7"))))
    val changeIds = changeLog.changes.map(_.asInstanceOf[AddPlaylist].id)
    val expected = Seq(Playlist("1234", "1", Seq("1")), Playlist("111111", "1", Seq("1","3"))).sortBy(_.id)
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    val r = logProcessor.result
    // ensure that result is naturally sorted by playlist_id
    val r1 = r.playlists.sortBy(_.id)
    assert(r.playlists.corresponds(r1)(_ == _))

    val res = (r.playlists.filter(p=>changeIds.contains(p.id)))
    assert(
      res.corresponds(expected)(_ == _)
    )
  }

  test("Add same playlists in the end") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(AddPlaylist("9999", "1", Seq("1")), AddPlaylist("98", "1", Seq("1","3")), AddPlaylist("111111", "1", Seq("1","3","7"))))
    val changeIds = changeLog.changes.map(_.asInstanceOf[AddPlaylist].id)
    val expected = Seq(Playlist("9999", "1", Seq("1")), Playlist("98", "1", Seq("1","3")), Playlist("111111", "1", Seq("1","3","7"))).sortBy(_.id)
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    val r = logProcessor.result
    // ensure that result is naturally sorted by playlist_id
    val r1 = r.playlists.sortBy(_.id)
    assert(r.playlists.corresponds(r1)(_ == _))

    val res = (r.playlists.filter(p=>changeIds.contains(p.id)))
    assert(
      res.corresponds(expected)(_ == _)
    )
  }

  test("Delete non-existent playlist") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(RemovePlaylist("9999")))
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    val r = logProcessor.result
    // ensure that result is naturally sorted by playlist_id
    val r1 = r.playlists.sortBy(_.id)
    assert(r.playlists.corresponds(r1)(_ == _))
    assert(dataset.playlists.sortBy(_.id).corresponds(r.playlists)(_ == _))
  }

  test("Delete existent playlist") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(RemovePlaylist("2")))
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    val r = logProcessor.result
    // ensure that result is naturally sorted by playlist_id
    val r1 = r.playlists.sortBy(_.id)
    assert(r.playlists.corresponds(r1)(_ == _))
    assert(!r.playlists.exists(_.id == "2"))
  }


  test("Add and remove  playlists") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(AddPlaylist("9999", "1", Seq("1")), AddPlaylist("98", "1", Seq("1","3")),
        AddPlaylist("111111", "1", Seq("1","3","7")),RemovePlaylist("98")))
    val changeLog1 =
      ChangeLog(Seq(RemovePlaylist("2")))
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    logProcessor.applyLog(changeLog1)
    val r = logProcessor.result
    // ensure that result is naturally sorted by playlist_id
    val r1 = r.playlists.sortBy(_.id)
    assert(r.playlists.corresponds(r1)(_ == _))
    assert(!r.playlists.exists(_.id == "2"))
    assert(!r.playlists.exists(_.id == "98"))
  }


  test("Add songs to added playlist") {
    val dataset = FileParser.getDataset(mixtape)
    // only adds 1 and 3
    val changeLog =
      ChangeLog(Seq(AddPlaylist("9999", "1", Seq("1")),AddSong("2","9999")))
    val logProcessor = new LogProcessor(dataset)
    logProcessor.applyLog(changeLog)
    val r = logProcessor.result
    // ensure that result is naturally sorted by playlist_id
    val r1 = r.playlists.sortBy(_.id)
    assert(r.playlists.corresponds(r1)(_ == _))
    assert(!r.playlists.exists(_.id == "98"))
    assert(r.playlists.find(_.id == "9999").get.song_ids.sorted == Seq("1","2"))
  }
}

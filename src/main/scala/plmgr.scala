package plmgr

import java.io.PrintWriter
import spray.json._

import JsonProtocolDataset._

object PlayListMgr extends App {
  if (args.length != 3) {
    println("Usage: plmgr <dataset-file> <changes-file> <output-file>")
    System.exit(1)
  }

  val dataset = FileParser.getDataset(args(0))
  val changeLog = FileParser.getChangelog(args(1))

  // Apply changes
  val logProcessor = new LogProcessor(dataset)
  logProcessor.applyLog(changeLog)

  // Output
  val outputTxt = logProcessor.result.toJson.prettyPrint
  val pw = new PrintWriter(args(2))
  pw.write(outputTxt)
  pw.close()
}

package plmgr

import com.typesafe.scalalogging.LazyLogging
import scala.io.Source
import spray.json._
import scala.annotation.tailrec
import scala.util.{Try}

// Dataset file objects
case class Playlist(id: String, owner_id: String, song_ids: Seq[String])
case class User(id: String, name: String)
case class Song(id: String, artist: String, title: String)
case class Dataset(songs: Seq[Song], playlists: Seq[Playlist], users: Seq[User])

// change actions
sealed trait Change
case class AddSong(id: String, playlist_id: String) extends Change
case class AddPlaylist(id: String, user_id: String, song_ids: Seq[String])
    extends Change
case class RemovePlaylist(id: String) extends Change
case class ChangeLog(changes: Seq[Change])

object JsonProtocolDataset extends DefaultJsonProtocol {
  implicit val playlist = jsonFormat3(Playlist)
  implicit val usr = jsonFormat2(User)
  implicit val song = jsonFormat3(Song)
  implicit val datasetf = jsonFormat3(Dataset)
}

object JsonProtocolChangeLog extends DefaultJsonProtocol {
  implicit val addSong = jsonFormat2(AddSong)
  implicit val addPlaylist = jsonFormat3(AddPlaylist)
  implicit val removePlaylist = jsonFormat1(RemovePlaylist)
  implicit object ChangeFormat extends JsonFormat[Change] {
    override def read(json: JsValue): Change = json match {
      case known: JsObject if known.fields.contains("add_song") =>
        known.fields.get("add_song").get.convertTo[AddSong]
      case known: JsObject if known.fields.contains("add_playlist") =>
        known.fields.get("add_playlist").get.convertTo[AddPlaylist]
      case known: JsObject if known.fields.contains("rm_playlist") =>
        known.fields.get("rm_playlist").get.convertTo[RemovePlaylist]
      case unknown => deserializationError(s"unknown ChangeObject: $unknown")
    }

    // do not need to serialize changes, no need to implement
    override def write(obj: Change): JsValue = ???
  }

  implicit val changeLogf = jsonFormat1(ChangeLog)

}

import JsonProtocolDataset._

import JsonProtocolChangeLog._

object FileParser {
  def getDataset(file: String): Dataset = {
    // Read Dataset file
    val lines = Try { Source.fromFile(file) }
      .getOrElse(throw new Exception(s"Failed to open file ${file}"))
      .getLines
      .toList
      .mkString
    Try { lines.parseJson.convertTo[Dataset] }
      .getOrElse(throw new Exception("Failed to parse dataset file"))
  }
  def getChangelog(file: String): ChangeLog = {

    // Read changes file
    val lines1 = Try { Source.fromFile(file) }
      .getOrElse(throw new Exception(s"Failed to open file ${file}"))
      .getLines
      .toList
      .mkString
    Try { lines1.parseJson.convertTo[ChangeLog] }
      .getOrElse(throw new Exception("Failed to parse changes file"))
  }
}

class LogProcessor(dataset: Dataset) extends LazyLogging {
  // convert users, songs to read-only map for easy lookup
  // in real scenario they can be stored in a SQL db with cache (memcached) for fast lookup
  private val users = dataset.users.map(p => (p.id, p)).toMap
  private val songs = dataset.songs.map(p => (p.id, p)).toMap

  // assume that # of playlist entries and change log entries are very large, so process them in a streaming fashion
  // assume that playlists is a stream sorted by playlist_id
  private var playlists = dataset.playlists.sortBy(_.id)

  def applyLog(changeLog: ChangeLog): Unit = {
    // assume that all action are "sharded" or grouped by playlist_id
    // use foldRight below to "shard" or group by playlist_id all changelog actions
    // foldRight preserves chronological order of actions {pl1,[a1,a3]},{pl2,[a2,a4]}
    val changes = changeLog.changes
      .foldRight(scala.collection.mutable.Map.empty[String, Seq[Change]]) {
        case (p, z) =>
          p match {
            case a: AddSong =>
              val v = a +: z.getOrElse(a.playlist_id, Seq.empty)
              z.update(a.playlist_id, v)
              z
            case a: AddPlaylist =>
              val v = a +: z.getOrElse(a.id, Seq.empty)
              z.update(a.id, v)
              z
            case a: RemovePlaylist =>
              val v = a +: z.getOrElse(a.id, Seq.empty)
              z.update(a.id, v)
              z
          }
      }
      .toList
      .sortBy(_._1) // sort by playlist_id
      .flatMap(p => p._2.map(t => (p._1, t))) // convert to list of pairs (playlist_id, change)

    // apply changes
    playlists = loop(playlists, changes, Seq.empty).reverse
  }

  // apply "sharded" stream of changelog to a "stream" of playlists sorted by playlist_id
  // E.g [pl1,pl2,pl3] [(pl1,a1),(pl1,a2),(pl3,a1)]
  // Merge them just like in merge sort
  @tailrec // make sure that the loop is tail recursive
  private def loop(ps: Seq[Playlist], // original playlist
                   cs: Seq[(String, Change)], // changes to apply
                   acc: Seq[Playlist] // accumulator with final result
  ): Seq[Playlist] = {
    (ps, cs) match {
      case (Nil, Nil) => acc // both lists are empty, terminate loop
      case (h :: t, Nil) =>
        loop(t, cs, h +: acc) // changelog list is empty, prepend one by one pl elements

      // playlist is empty, populate it from changelog, since some changelog actions might be applied
      // to newly added elements
      case (Nil, h :: t) =>
        loop(applyAction(None, h._2) match {
          case Some(p) => p +: ps
          case _       => ps
        }, t, acc)

      // playlist id from playlists stream is still not reached the first playlist id in the change log
      // prepend playlist item to the result, no action is needed
      case (h1 :: t1, h2 :: _) if h1.id < h2._1 => loop(t1, cs, h1 +: acc)

      // playlist id from playlists stream is beyond current playlist id in change log
      // it must be AddPlaylist action, apply it and prepend result to ps, since there might be more actions
      // in change log against this entry
      case (h1 :: _, h2 :: t2) if h1.id > h2._1 =>
        loop(applyAction(None, h2._2) match {
          case Some(p) => p +: ps
          case _       => ps
        }, t2, acc)

      // playlist ids match. It is either AddSong or RemovePlaylist
      // apply action and keep the result in ps list, since there might be more actions against this playlist_id in the changelog
      case (h1 :: t1, h2 :: t2) if h1.id == h2._1 =>
        loop(applyAction(Some(h1), h2._2) match {
          case Some(p) => p +: t1
          case _       => t1
        }, t2, acc)
    }
  }

  // apply change log action to a playlist item
  // input playlist item can be None (AddPlayList action)
  // if not None, then it is AddSong or RemovePlaylist action
  private def applyAction(pl: Option[Playlist], a: Change): Option[Playlist] = {
    a match {
      case a: AddSong =>
        if (!songs.contains(a.id)) {
          logger.warn(
            s"Cannot apply action $a to a playlist: song does not exist")
          pl
        } else {
          pl match {
            // playlist_id match, then apply action
            case Some(p) if p.id == a.playlist_id =>
              if (p.song_ids.contains(a.id)) {
                logger.warn(
                  s"Cannot apply action $a to a playlist: song already exists")
                Some(p)
              } else {
                logger.info(s"Added song ${a.id} to playlist ${p.id}")
                Some(p.copy(song_ids = p.song_ids :+ a.id))
              }

            case _ =>
              logger.warn(
                s"Cannot apply action $a to a playlist: playlist does not exist")
              None
          }
        }
      case a: AddPlaylist =>
        // playlist must not be empty
        if (a.song_ids.isEmpty) {
          logger.warn(
            s"Cannot apply action $a to a playlist: play must contain at leats one song")
          None
        } else if (!users.contains(a.user_id)) {
          logger.warn(
            s"Cannot apply action $a to a playlist: user does not exist")
          None
        } else {
          logger.info(s"Added playlist ${a.id}.")
          pl match {
            case None => Some(Playlist(a.id, a.user_id, a.song_ids))
            case Some(p) =>
              logger.warn(
                s"Cannot apply action $a to a playlist: playlist already exists")
              Some(p)
          }
        }

      case a: RemovePlaylist =>
        logger.info(s"Removed playlist ${a.id}.")
        pl match {
          case Some(p) if (p.id == a.id) => None
          case None =>
            logger.warn(s"Cannot remove playlist ${a.id}. It doesn't exists")
            None
        }
    }
  }

  def result: Dataset = dataset.copy(playlists = playlists)

}
